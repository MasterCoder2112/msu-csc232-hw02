# Homework Assignment #2

## Chapter 2 Recursion - Implementing the Aker function using recursion

## Due: 23:59 Friday 17 February 2017

## Points: 5

This assignment is based upon Exercise 26 found in Chapter 2. In the usual manner, fork this repo into your own private repo and clone it to your local machine. Once cloned, checkout the develop branch and make sure you can compile and run this program. Open a terminal window and navigate to the `src\release` sub-directory and type the following:

```bash
$ g++ -std=c++14 -Wall Main.cpp -o hw02
$ ./hw02
Acker(1, 2) = *** replace me with call to Acker***
$
```

Clearly this is not the expected output.

Alternatively, you can navigate into the `build/unix` sub-directory and build the executable using a cmake generated Makefile. See the [README](build/unix/README.md) in that folder for more details.

Now that you have compiled and executed this program, open `Main.cpp` and follow the instructions put for by the various `TODO` comments. When you have finished the last `TODO`, create a pull request to merge your develop branch into your master branch and provide me with the URL of this pull request in a Text Submission to the assignment found on Blackboard.

As a reminder, all homework assignments are to be individual efforts.