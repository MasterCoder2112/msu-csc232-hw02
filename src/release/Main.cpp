/**
 * CSC232 Data Structures with C++

 * Missouri State University, Spring 2017.
 *
 * @file    Main.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Alexander Byrd <zz2112@live.missouristate.edu>
 *          
 * @brief   Entry point to this application.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#include <cstdlib>
#include <iostream>

/**
 * @title Acker
 * @pre An integer m and n are sent int the function and must be greater
 * than 0.
 * @post Sends back an integer, even through I really don't know what this
 * integer symbolizes. m and n are not changed.
 * @return An integer
 * @parameters const int m, const int n
 * @brief Takes an m and n sent in and figures out what to send back
 * depending on what m and n are. If m = 0 it returns n + 1. If n = 0
 * it calls Acker(m - 1, 1) until it reaches m = 0. And if both are
 * greater than 0 it calls Acker(m - 1, Acker(m, n-1)) until it gets
 * to m = 0.
 */
int Acker(const int m, const int n);

/**
 * @title NegativeParameterException
 * @brief Sets up a new runtime_error called the NegativeParameterException
 * which is called whenever a negative parameter is sent to the Acker
 * function so that negative values cannot be allowed to be sent in.
 */
class NegativeParameterException: public std::runtime_error {

public:
   NegativeParameterException():
   std::runtime_error("Negative Parameter sent in!" ) {}
};

/**
 * @brief Entry point to this application.
 * @remark You are encouraged to modify this file as you see fit to gain
 * practice in using objects.
 *
 * @param argc the number of command line arguments
 * @param argv an array of the command line arguments
 * @return EXIT_SUCCESS upon successful completion
 */
int main(int argc, char **argv) {

	//Tries to call Acker, but if there is a negative parameter throw
	//an exception and print it to the screen, discontinuing the program.
	try {

		std::cout << Acker(1, 2) << std::endl;
	}
	catch(NegativeParameterException &e) {

		std::cerr << "Exception occurred: " << e.what() << std::endl;
	}

    return EXIT_SUCCESS;
}

/**
 * Function doxygen given in prototype
 */
int Acker(const int m, const int n) {

	//Throw exception if a parameter is negative
	if(m < 0 || n < 0) {

		throw NegativeParameterException();
	}

	if(m == 0) {
		return n + 1;
	}
	else if(n == 0) {
		return Acker(m - 1, 1);
	}
	else {
		return Acker(m - 1, Acker(m, n - 1));
	}
}

